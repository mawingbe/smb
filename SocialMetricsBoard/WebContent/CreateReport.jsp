<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="/style/main.css" rel="stylesheet">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Create Report</title>
<style type="text/css">
body{
	font-family: "Open Sans";
}

h1 {
 text-align: center;
 color: black;
 background: #F8973C ;
 font-family: "Open Sans";
 }
</style>
</head>
<body>
	<form action="CreatorServlet" method="get">
		<p>
			ArticleURL <input type="text" name="ArticleURL"
				value="http://beta.himate.de/2015/04/10-vorteile-die-du-nur-hast-wenn-du-russische-freunde-hast/">
		</p>

		<p>
			PostURL <input type="text" name="PostURL"
				value="https://www.facebook.com/himateDE/posts/811738048908727">
		</p>

		<input type="submit" value="Create Report">
	</form>
	<% if(session.getAttribute("LastError")!=null){ %>
	<p>Fehler: <%= session.getAttribute("LastError")   %>
	<% session.removeAttribute("LastError");} %>

</body>
</html>