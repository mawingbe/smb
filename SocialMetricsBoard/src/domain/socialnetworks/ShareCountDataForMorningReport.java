package domain.socialnetworks;

//Data Class
public class ShareCountDataForMorningReport {

	private int fb_shares;
	private int fb_comments;
	private int fb_likes;
	private int plusones;
	private int tweets;
	public ShareCountDataForMorningReport(int fb_shares, int fb_comments,
			int fb_likes, int plusones, int tweets) {
		super();
		this.fb_shares = fb_shares;
		this.fb_comments = fb_comments;
		this.fb_likes = fb_likes;
		this.plusones = plusones;
		this.tweets = tweets;
	}
	public int getFb_shares() {
		return fb_shares;
	}
	public int getFb_comments() {
		return fb_comments;
	}
	public int getFb_likes() {
		return fb_likes;
	}
	public int getPlusones() {
		return plusones;
	}
	public int getTweets() {
		return tweets;
	}
	
	
}
