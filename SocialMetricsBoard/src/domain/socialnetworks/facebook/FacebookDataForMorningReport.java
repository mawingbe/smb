package domain.socialnetworks.facebook;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class FacebookDataForMorningReport {

	private double ctr;
	private String postURL;
	private String imageURL;
	private String headline;

	public FacebookDataForMorningReport(int postconsumption, int postreach,
			String postURL, String imageURL, String headline) {
		super();
		ctr = (double) postconsumption / (double) postreach;
		ctr = round(ctr*100, 2);
		this.postURL = postURL;

		this.imageURL = imageURL;
		this.headline = headline;
	}

	public double getCtr() {
		return ctr;
	}

	public void setCtr(double ctr) {
		this.ctr = ctr;
	}

	public String getPostURL() {
		return postURL;
	}

	public void setPostURL(String postURL) {
		this.postURL = postURL;
	}

	public String getImageURL() {
		return imageURL;
	}

	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}

	public String getHeadline() {
		return headline;
	}

	public void setHeadline(String headline) {
		this.headline = headline;
	}

	@Override
	public String toString() {
		return "FacebookDataForMorningReport [ctr="
				+ ctr +  ", postURL="
				+ postURL + ", imageURL=" + imageURL + ", headline=" + headline
				+ "]";
	}

	private double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    BigDecimal bd = new BigDecimal(value);
	    bd = bd.setScale(places, RoundingMode.HALF_UP);
	    return bd.doubleValue();
	}
	
}
