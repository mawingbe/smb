package domain.socialnetworks.facebook;

public class FQLRequest {
	private String query;

	public FQLRequest(String query) {
		super();
		this.query = query;

	}

	public String getQuery() {
		return query;
	}

}
