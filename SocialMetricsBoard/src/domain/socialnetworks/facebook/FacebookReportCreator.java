package domain.socialnetworks.facebook;

import java.io.IOException;

import java.util.List;

import org.jsoup.Jsoup;

import service.FQLHandler;
import access.FacebookAccess;

import com.restfb.FacebookClient;

import facebook.FQLPostInsightResponse;
import facebook.PostInsightMetric;

public class FacebookReportCreator {

	public FacebookDataForMorningReport getReportData(String postURL,
			String URL) throws IOException {
		//Post-ID consists of the last 15 chars of Post-URL
		String objectID = FacebookAccess.getPageID() + "_"
				+ postURL.subSequence(postURL.length() - 15, postURL.length());
	
		FacebookClient facebookClient = FacebookAccess.getFacebookClient();

		// Class to handle easy FQL calls
		FQLHandler fqlHandler = new FQLHandler(facebookClient);

		int consumptions = consumptions(objectID, fqlHandler);
		int impressions = impressions(objectID, fqlHandler);

		String headline = "";
		String imageurl = "";
	
			org.jsoup.nodes.Document docx = Jsoup.connect(URL).get();
			for (org.jsoup.nodes.Element meta : docx.select("meta")) {
				if (meta.attr("property").equals("og:title")) {

					headline = meta.attr("content");
				}
				if (meta.attr("property").equals("og:image")) {
					imageurl = meta.attr("content");
				}
			}

	

		FacebookDataForMorningReport data = new FacebookDataForMorningReport(
				consumptions, impressions, postURL, imageurl, headline);

		return data;

	}

	private int consumptions(String objectID, FQLHandler bob) {
		String query = "SELECT metric, value FROM insights WHERE object_id= '"
				+ objectID + "' " + "AND metric='"
				+ PostInsightMetric.post_consumptions + "' AND period=0";
		List<FQLPostInsightResponse> result = bob
				.getPostInsights(new FQLRequest(query));
		int consumptions = result.get(0).getValue();
		return consumptions;
	}

	private int impressions(String objectID, FQLHandler bob) {
		String query = "SELECT metric, value FROM insights WHERE object_id= '"
				+ objectID + "' " + "AND metric='"
				+ PostInsightMetric.post_impressions + "' AND period=0";
		List<FQLPostInsightResponse> result = bob
				.getPostInsights(new FQLRequest(query));
		int impressions = result.get(0).getValue();
		return impressions;
	}

}
