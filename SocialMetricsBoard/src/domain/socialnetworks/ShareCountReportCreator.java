package domain.socialnetworks;

import java.io.IOException;

import org.json.JSONObject;

import service.URLHandler;

public class ShareCountReportCreator {
	private static URLHandler urlhandler;
	private final static String APIKEY = "9cf902e56b8df2585e127a4cfc5fa2f76f1c6cbc";

	public ShareCountReportCreator() {
		super();
		// Class to execute URL Requests
		urlhandler = new URLHandler();
	}

	public static ShareCountDataForMorningReport getReportData(String URL) {
		String requestUrl = "http://free.sharedcount.com/?url=" + URL
				+ "&apikey=" + APIKEY;
		try {
			String json = urlhandler.readNormalUrlGET(requestUrl);
			JSONObject content = new JSONObject(json);
			// Extract data from JSON Object
			JSONObject facebook = content.getJSONObject("Facebook");
			int plusones = (int) content.getInt("GooglePlusOne");
			int fb_shares = facebook.getInt("share_count");
			int fb_comments = facebook.getInt("comment_count");
			int fb_likes = facebook.getInt("like_count");
			int tweets = (int) content.getInt("Twitter");
			return new ShareCountDataForMorningReport(fb_shares, fb_comments,
					fb_likes, plusones, tweets);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
}
