package domain.googleanalytics;

import java.math.BigDecimal;
import java.math.RoundingMode;


//Data-class
public class AnalyticsDataForMorningReport {

	private int pageviews;
	private double scrollrateFifty;
	private double scrollrateSeventyFive;
	private int averageTimeSpent;
	private double bounceRate;
	private int pagesize;

	public AnalyticsDataForMorningReport(int pageviews, int pageviewsBounced,
			int baseline, int fifty, int seventyfive, int timeSpent,
			int pagesize) {
		super();
		this.pageviews = pageviews;
		this.pagesize = pagesize;
		fillData(pageviews, pageviewsBounced, baseline, fifty, seventyfive,
				timeSpent);
	}

	public double getBounceRate() {
		return bounceRate;
	}

	public int getPageviews() {
		return pageviews;
	}

	public int getPagesize() {
		return pagesize;
	}

	public double getScrollrateFifty() {
		return scrollrateFifty;
	}

	public double getScrollrateSeventyFive() {
		return scrollrateSeventyFive;
	}

	public int getAverageTimeSpent() {
		return averageTimeSpent;
	}

	private double round(double value, int places) {
		if (places < 0)
			throw new IllegalArgumentException();

		BigDecimal bd = new BigDecimal(value);
		bd = bd.setScale(places, RoundingMode.HALF_UP);
		return bd.doubleValue();
	}

	private void fillData(int pageviews, int pageviewsBounced, int baseline,
			int fifty, int seventyfive, int timeSpent) {
		scrollrateFifty = (double) fifty / (double) baseline;
		scrollrateFifty = round(scrollrateFifty * 100, 2);
		scrollrateSeventyFive = (double) seventyfive / (double) baseline;
		scrollrateSeventyFive = round(scrollrateSeventyFive * 100, 2);
		averageTimeSpent = timeSpent / pageviews;
		bounceRate = (double) pageviewsBounced / (double) pageviews;
		bounceRate = round(bounceRate * 100, 2);

	}
}
