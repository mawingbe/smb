package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import service.DocPrinter;

/**
 * Servlet implementation class CreatorServlet
 */
@WebServlet(
        name = "CreatorServlet",
        urlPatterns = {"/CreatorServlet"}
    )
public class CreatorServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CreatorServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		DocPrinter printer = new DocPrinter();
		try {
			printer.printDoc(request, response);
		} catch (Exception e) {
			System.out.println("Could not print");
			e.printStackTrace();
			request.getSession().setAttribute("LastError", "Wrong URL");
			response.sendRedirect("CreateReport.jsp");
		}

		// Document doc = XMLCreator.createXML(request.getParameter("PostURL"),
		// request.getParameter("ArticleURL"));
		// response.setContentType("text/xhtml");
		// response.setHeader("Content-disposition",
		// "attachment; filename=export.xhtml");

		// response.setContentType("application/pdf");
		// response.setHeader("Content-disposition",
		// "attachment; filename=export.pdf");

		// try {
		// InputStream xslt = this.getClass().getResourceAsStream(
		// "/xhtml.xslt");
		// Transformer transformer = TransformerFactory.newInstance()
		// .newTransformer(new StreamSource(xslt));
		// DOMSource domSource = new DOMSource(doc);
		// transformer.transform(domSource,
		// new StreamResult(response.getOutputStream()));
		//
		// } catch (TransformerConfigurationException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// } catch (TransformerFactoryConfigurationError e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// } catch (TransformerException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
