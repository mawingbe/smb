package facebook;

import com.restfb.Facebook;

public class FQLPostInsightResponse {
	@Facebook
	String metric;

	@Facebook
	int value;

	@Override
	public String toString() {
		return "Metric=" + metric + ", Value=" + value;
	}

	public String getMetric() {
		return metric;
	}

	public int getValue() {
		return value;
	}

}