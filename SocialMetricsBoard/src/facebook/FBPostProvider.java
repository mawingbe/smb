package facebook;

import java.util.Date;
import java.util.List;

import com.restfb.Connection;
import com.restfb.FacebookClient;
import com.restfb.Parameter;
import com.restfb.types.Post;

public class FBPostProvider {

	FacebookClient facebookClient;

	public FBPostProvider(FacebookClient facebookClient) {
		super();
		this.facebookClient = facebookClient;
	}

	/**
	 * 
	 * @param page
	 * @param amount
	 * @return returns a chosen amount of posts from a page
	 */
	public List<Post> getPosts(String page, int amount) {
		page += "/feed";
		Connection<Post> myFeed = facebookClient.fetchConnection(page,
				Post.class, Parameter.with("limit", amount));
		List<Post> feedData = myFeed.getData();
		return feedData;
	}

	/**
	 * 
	 * @param page
	 * @param since
	 * @param until
	 * @return returns post from a page for a specified period
	 */
	public List<Post> getPosts(String page, Date since, Date until) {
		page += "/feed";
		Connection<Post> myFeed = facebookClient.fetchConnection(page,
				Post.class, Parameter.with("since", since),
				Parameter.with("until", until));
		List<Post> feedData = myFeed.getData();
		return feedData;
	}

	/**
	 * 
	 * @param page
	 * @return returns the posts of a page for the current month
	 */
	@SuppressWarnings("deprecation")
	public List<Post> getPosts(String page) {
		Date now = new Date(System.currentTimeMillis());
		Date firstOfMonth = new Date(now.getYear(), now.getMonth(), 1);

		page += "/feed";
		Connection<Post> myFeed = facebookClient.fetchConnection(page,
				Post.class, Parameter.with("since", firstOfMonth),
				Parameter.with("until", now));
		List<Post> feedData = myFeed.getData();
		return feedData;

	}

}
