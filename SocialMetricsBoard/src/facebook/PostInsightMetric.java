package facebook;

public enum PostInsightMetric {
	post_consumptions, post_impressions, post_impressions_paid, post_impressions_fan, post_impressions_organic, post_impressions_viral
}
