package reworked;

import java.io.IOException;
import java.util.HashMap;

import com.google.api.client.auth.oauth2.AuthorizationCodeFlow;
import com.google.api.client.auth.oauth2.AuthorizationCodeRequestUrl;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.auth.oauth2.TokenResponse;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.java6.auth.oauth2.VerificationCodeReceiver;

public class AuthorizationCodeWebApp extends AuthorizationCodeInstalledApp {
	private String authURL;
	private HashMap<String, String> urls;
	private AuthorizationCodeFlow flow;
	private VerificationCodeReceiver receiver;

	public AuthorizationCodeWebApp(AuthorizationCodeFlow flow,
			VerificationCodeReceiver receiver) {
		super(flow, receiver);
		this.receiver=receiver;
		this.flow=flow;
		// TODO Auto-generated constructor stub
	}

	public String getAuthorizationURL() throws IOException {
		String redirectUri = receiver.getRedirectUri();
		AuthorizationCodeRequestUrl authorizationUrl = flow
				.newAuthorizationUrl().setRedirectUri(redirectUri);
		authURL = authorizationUrl.build();
		return authURL;
	}

	public Credential authorize(String userId) throws IOException {
		// already authorized
		String redirectUri = receiver.getRedirectUri();
		try {
			Credential credential = flow.loadCredential(userId);
			if (credential != null
					&& (credential.getRefreshToken() != null || credential
							.getExpiresInSeconds() > 60)) {
				return credential;
			}

			// receive authorization code and exchange it for an access token
			String code = receiver.waitForCode();
			TokenResponse response = flow.newTokenRequest(code)
					.setRedirectUri(redirectUri).execute();
			// store credential and return it
			return flow.createAndStoreCredential(response, userId);
		} finally {
			receiver.stop();
		}
	}

	protected void onAuthorization(AuthorizationCodeRequestUrl authorizationUrl)
			throws IOException {
		authURL = authorizationUrl.build();
		browse(authURL);
	}

}
