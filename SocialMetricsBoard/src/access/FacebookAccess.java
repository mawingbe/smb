package access;

import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.Version;

public class FacebookAccess {

	private static String ACCESS_TOKEN = null;
	private static final String pageID = "617496371666230";
	private static FacebookClient facebookClient;

	public FacebookAccess(String accesstoken) {
		super();
		ACCESS_TOKEN = accesstoken;
		//Create facebook client via RestFB-API
		facebookClient = new DefaultFacebookClient(ACCESS_TOKEN,
				Version.VERSION_2_0);
		// TODO Auto-generated constructor stub
	}

	public static FacebookClient getFacebookClient() {
		return facebookClient;
	}

	public static String getPageID() {
		return pageID;
	}

}
