package access;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

import reworked.AuthorizationCodeWebApp;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.analytics.Analytics;
import com.google.api.services.analytics.AnalyticsScopes;
import com.google.api.services.pagespeedonline.Pagespeedonline.Builder;

public class AnalyticsAccessNew {
	private final String CLIENT_SECRET_JSON_RESOURCE = "client_secrets.json";
	private Builder builder;
	private Analytics analytics;
	// The ID of the Analytics profile
	private final static String PROFILE_ID = "89360927";

	// The directory where the user's credentials will be stored.(Local)
	private final File DATA_STORE_DIR = new File(
			System.getProperty("user.home"), "/hello_analytics");

	private final String APPLICATION_NAME = "Social Metrics Board";
	private final JsonFactory JSON_FACTORY = GsonFactory.getDefaultInstance();
	private NetHttpTransport httpTransport;
	private FileDataStoreFactory dataStoreFactory;
	private AuthorizationCodeWebApp acwa;
	private Credential credential;

	public AnalyticsAccessNew() {

	}

	public String initializeAnalytics() throws Exception {
		// rwx-rights for stored file
		System.out.println("Set RWX");
		DATA_STORE_DIR.setExecutable(true);
		DATA_STORE_DIR.setReadable(true);
		DATA_STORE_DIR.setWritable(true);

		System.out.println("Instantiate transport via http");
		// Instantiate transport via http
		httpTransport = GoogleNetHttpTransport.newTrustedTransport();
		System.out.println("create dataStoreFactory from local directory");
		// create dataStoreFactory from local directory
		dataStoreFactory = new FileDataStoreFactory(DATA_STORE_DIR);

		System.out.println("Load client secrets.");
		// Load client secrets.
		GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(
				JSON_FACTORY, new InputStreamReader(this.getClass()
						.getResourceAsStream(CLIENT_SECRET_JSON_RESOURCE)));

		System.out
				.println(" Set up authorization code flow for all auth scopes.");
		// Set up authorization code flow for all auth scopes.
		GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
				httpTransport, JSON_FACTORY, clientSecrets,
				AnalyticsScopes.all()).setDataStoreFactory(dataStoreFactory)
				.build();

		System.out.println("authorize");
		// Authorize.

		LocalServerReceiver localServerReceiver = new LocalServerReceiver();
		acwa = new AuthorizationCodeWebApp(flow, localServerReceiver);
		String authURL = acwa.getAuthorizationURL();
		return authURL;
	}

	public Analytics finishInitializingAnalytics() throws Exception {
		credential = acwa.authorize("user");

		System.out.println("create builder");
		// Create Builder
		builder = new Builder(httpTransport, JSON_FACTORY,
				setHttpTimeout(credential));
		builder.setApplicationName(APPLICATION_NAME);

		System.out.println("construct analytics service object");
		// Construct the Analytics service object.
		return new Analytics.Builder(httpTransport, JSON_FACTORY, credential)
				.setApplicationName(APPLICATION_NAME)
				.setHttpRequestInitializer(setHttpTimeout(credential)).build();
	}

	private HttpRequestInitializer setHttpTimeout(
			final HttpRequestInitializer requestInitializer) {
		return new HttpRequestInitializer() {
			@Override
			public void initialize(HttpRequest httpRequest) throws IOException {
				requestInitializer.initialize(httpRequest);
				httpRequest.setConnectTimeout(3 * 60000); // 3 minutes connect
															// timeout
				httpRequest.setReadTimeout(3 * 60000); // 3 minutes read timeout
			}
		};
	}

	public Analytics getAnalytics() {
		return analytics;
	}

	public static String getPROFILE_ID() {
		return PROFILE_ID;
	}

	public Builder getBuilder() {
		System.out.println("get builder");
		return builder;
	}

}
