package service;

import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.jsoup.Jsoup;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import domain.googleanalytics.AnalyticsDataForMorningReport;
import domain.googleanalytics.AnalyticsReportCreator;
import domain.socialnetworks.facebook.FacebookDataForMorningReport;
import domain.socialnetworks.facebook.FacebookReportCreator;

public class XMLCreator {
	AnalyticsReportCreator arc;
	FacebookReportCreator facebookReportCreator;

	public XMLCreator() {
		super();
		arc = new AnalyticsReportCreator();
		facebookReportCreator = new FacebookReportCreator();

		// TODO Auto-generated constructor stub
	}

	public Document createXML(String postURL, String URL) throws IOException {
		System.out.println("Fetch Facebook Data");
		FacebookDataForMorningReport fbdata = facebookReportCreator
				.getReportData(postURL, URL);
		System.out.println("Fetch Analytics Data");
		AnalyticsDataForMorningReport adata = arc.getReportData(URL);
		try {
			System.out.println("Build Document");
			DocumentBuilder db = DocumentBuilderFactory.newInstance()
					.newDocumentBuilder();
			Document doc = db.newDocument();
			Element report = doc.createElement("REPORT");

			Element header = createHeader(fbdata, doc, URL);
			Element facebookdata = createFacebookData(fbdata, doc, URL);
			Element analytics = createAnalytics(doc, adata);

			report.appendChild(header);
			report.appendChild(facebookdata);
			report.appendChild(analytics);

			doc.appendChild(report);
			return doc;
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block

			e.printStackTrace();
			return null;
		}

	}

	private Element createAnalytics(Document doc,
			AnalyticsDataForMorningReport adata) {
		Element analytics = doc.createElement("ANALYTICS");
		Element pageviews = doc.createElement("PAGEVIEWS");
		pageviews.setTextContent(new Integer(adata.getPageviews()).toString());
		Element fifty = doc.createElement("FIFTY");
		fifty.setTextContent(new Double(adata.getScrollrateFifty()).toString());
		Element seventyfive = doc.createElement("SEVENTYFIVE");
		seventyfive.setTextContent(new Double(adata.getScrollrateSeventyFive())
				.toString());
		Element timeSpent = doc.createElement("AVERAGETIMESPENT");
		timeSpent.setTextContent(new Integer(adata.getAverageTimeSpent())
				.toString());
		Element bounceRate = doc.createElement("BOUNCERATE");
		bounceRate.setTextContent(new Double(adata.getBounceRate()).toString());
		Element pagesize = doc.createElement("PAGESIZE");
		pagesize.setTextContent(new Integer(adata.getPagesize()).toString());

		analytics.appendChild(pageviews);
		analytics.appendChild(fifty);
		analytics.appendChild(seventyfive);
		analytics.appendChild(timeSpent);
		analytics.appendChild(bounceRate);
		analytics.appendChild(pagesize);

		return analytics;
	}

	private Element createFacebookData(FacebookDataForMorningReport data,
			Document doc, String URL) {
		Element facebookdata = doc.createElement("FACEBOOKDATA");
		Element postconsumption = doc.createElement("CTR");
		postconsumption.setTextContent(new Double(data.getCtr())

		.toString());

		Element imageurl = doc.createElement("IMAGEURL");
		imageurl.setTextContent(data.getImageURL());
		Element headline = doc.createElement("HEADLINE");
		headline.setTextContent(data.getHeadline());

		facebookdata.appendChild(postconsumption);
		facebookdata.appendChild(imageurl);
		facebookdata.appendChild(headline);
		return facebookdata;
	}

	private Element createHeader(FacebookDataForMorningReport data,
			Document doc, String aurl) {
		Element header = doc.createElement("HEADER");
		Element titel = doc.createElement("TITEL");
		titel.setTextContent(getTitle(aurl));
		Element url = doc.createElement("URL");
		url.setTextContent(aurl);
		header.appendChild(titel);
		header.appendChild(url);
		return header;
	}

	private String getTitle(String url) {
		try {
			org.jsoup.nodes.Document doc = Jsoup.connect(url).get();
			return doc.title();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

	}

}
