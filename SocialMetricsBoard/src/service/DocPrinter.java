package service;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.w3c.dom.Document;

public class DocPrinter {
	XMLCreator xmlCreator= new XMLCreator();
	

	public DocPrinter() {
		super();
		// TODO Auto-generated constructor stub
	}

	public void printDoc(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		Document doc = xmlCreator.createXML(request.getParameter("PostURL"),
				request.getParameter("ArticleURL"));
		// Download parameters
		response.setContentType("text/xhtml");
		response.setHeader("Content-disposition",
				"attachment; filename=export.xhtml");
		// response.setContentType("application/pdf");
		// response.setHeader("Content-disposition",
		// "attachment; filename=export.pdf");

		try {
			// Transform xml with xslt to xhtml
			InputStream xslt = this.getClass().getResourceAsStream(
					"/xhtml.xslt");
			Transformer transformer = TransformerFactory.newInstance()
					.newTransformer(new StreamSource(xslt));
			DOMSource domSource = new DOMSource(doc);
			// Put xhtml on servletoutput
			transformer.transform(domSource, new StreamResult(new FileOutputStream("text.xhtml")));
			transformer.transform(domSource,
					new StreamResult(response.getOutputStream()));
			

		} catch (TransformerConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransformerFactoryConfigurationError e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
