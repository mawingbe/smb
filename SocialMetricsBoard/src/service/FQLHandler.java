package service;

import java.util.List;

import com.restfb.FacebookClient;

import domain.socialnetworks.facebook.FQLRequest;
import facebook.FQLPostInsightResponse;

public class FQLHandler {

	private FacebookClient facebookclient;

	public FQLHandler(FacebookClient facebookclient) {
		super();
		this.facebookclient = facebookclient;
	}

	public List<FQLPostInsightResponse> getPostInsights(FQLRequest request) {
		return (List<FQLPostInsightResponse>) facebookclient.executeFqlQuery(
				request.getQuery(), FQLPostInsightResponse.class);
	}

}
