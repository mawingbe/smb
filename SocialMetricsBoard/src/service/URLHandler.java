package service;

/**
 * 
 * @author Max
 * 
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import org.jsoup.Jsoup;
import org.jsoup.select.Elements;

public class URLHandler {
	private String UserAgent = "Googlebot";

	public URLHandler() {
		super();
	}

	public String getUserAgent() {
		return UserAgent;
	}

	public void setUserAgent(String userAgent) {
		UserAgent = userAgent;
	}

	public int ResponseCode(String url) throws IOException {

		try {
			URL obj = new URL(url);
			// Open Connection to given URL
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			// Set Method, User-Agent and disable following redirects
			con.setRequestMethod("GET");
			con.setRequestProperty("User-Agent", UserAgent);
			con.setInstanceFollowRedirects(false);
			// Read out response code
			int responsecode = con.getResponseCode();
			System.out.println("Response Code= " + responsecode);
			return responsecode;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		throw new IOException("Couldn't get response code");
	}

	public boolean isOK(String url) {
		try {
			if (ResponseCode(url) == 200) {
				System.out.println(">>URL is ok");
				return true;
			} else {
				System.out.println(">>URL is not ok");
				return false;
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}

	}

	public String viewAuthor(String url) {
		try {

			org.jsoup.nodes.Document doc = Jsoup.connect(url).get();
			Elements author = doc.select("meta[name=author]");
			if (author != null) {
				String authorname = author.attr("content").toString();
				System.out.println(authorname);
				return authorname;

			} else {
				System.out.println(">>meta tag \"author\" not found");
			}

		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public String getTitleToURL(String url) {
		try {
			org.jsoup.nodes.Document doc = Jsoup.connect(url).get();
			return doc.title();
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	public String readNormalUrlGET(String urlString) throws IOException {
			BufferedReader reader = null;
		try {
			// Create URL from parameter
			URL url = new URL(urlString);
			// Initialize Input-Stream from URL
			reader = new BufferedReader(new InputStreamReader(url.openStream()));
			StringBuffer buffer = new StringBuffer();
			int read;
			char[] chars = new char[1024];
			while ((read = reader.read(chars)) != -1)
				buffer.append(chars, 0, read);

			return buffer.toString();
		} finally {
			reader.close();
		}
	}

	public String readHttpsUrlGET(String urlString) throws IOException {
		BufferedReader reader = null;
		try {
			// Create URL from parameter
			URL url = new URL(urlString);
			HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
			// Initialize Input-Stream from URL
			reader = new BufferedReader(new InputStreamReader(
					con.getInputStream()));
			StringBuffer buffer = new StringBuffer();
			int read;
			char[] chars = new char[1024];
			while ((read = reader.read(chars)) != -1)
				buffer.append(chars, 0, read);

			return buffer.toString();
		} finally {
			reader.close();
		}
	}

}
