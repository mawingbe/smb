package service;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.analytics.Analytics;
import com.google.api.services.analytics.AnalyticsScopes;
import com.google.api.services.analytics.model.GaData;


import com.google.api.services.pagespeedonline.Pagespeedonline.Builder;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

public class GoogleAnalyticsManager {
	private Builder builder;

	// Path to client_secrets.json file downloaded from the Developer's Console.
	// The path is relative to this class.java.

	private final String CLIENT_SECRET_JSON_RESOURCE = "client_secrets.json";

	// The ID of the Analytics profile
	private final String PROFILE_ID = "89360927";

	// The directory where the user's credentials will be stored.
	private final File DATA_STORE_DIR = new File(
			System.getProperty("user.home"), ".store/hello_analytics");

	private final String APPLICATION_NAME = "Social Metrics Board";
	private final JsonFactory JSON_FACTORY = GsonFactory.getDefaultInstance();
	private NetHttpTransport httpTransport;
	private FileDataStoreFactory dataStoreFactory;

	private Analytics analytics;

	public GoogleAnalyticsManager() {
		super();
		try {
			analytics = initializeAnalytics();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void testIt() {

		try {
			examplePrintResults(exampleGetResults(analytics));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Analytics getAnalytics() {
		return analytics;
	}

	private Analytics initializeAnalytics() throws Exception {
		DATA_STORE_DIR.setExecutable(true);
		DATA_STORE_DIR.setReadable(true);
		DATA_STORE_DIR.setWritable(true);

		httpTransport = GoogleNetHttpTransport.newTrustedTransport();
		dataStoreFactory = new FileDataStoreFactory(DATA_STORE_DIR);

		// Load client secrets.
		GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(
				JSON_FACTORY,
				new InputStreamReader(GoogleAnalyticsManager.class
						.getResourceAsStream(CLIENT_SECRET_JSON_RESOURCE)));

		// Set up authorization code flow for all auth scopes.
		GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
				httpTransport, JSON_FACTORY, clientSecrets,
				AnalyticsScopes.all()).setDataStoreFactory(dataStoreFactory)
				.build();

		// Authorize.
		Credential credential = new AuthorizationCodeInstalledApp(flow,
				new LocalServerReceiver()).authorize("user");
		
		
		builder= new Builder(
				httpTransport, JSON_FACTORY, setHttpTimeout(credential)	);
		
		

		// Construct the Analytics service object.
		return new Analytics.Builder(httpTransport, JSON_FACTORY, credential)
				.setApplicationName(APPLICATION_NAME)
				.setHttpRequestInitializer(setHttpTimeout(credential)).build();
	}
	
	public Builder getbuilder (){
		return builder;
	}

	public GaData exampleGetResults(Analytics analytics) throws IOException {
		// Query the Core Reporting API for the number of sessions
		// in the past seven days.
		return analytics.data().ga()
				.get("ga:" + PROFILE_ID, "2014-08-01", "today", "ga:sessions")
				.execute();
	}

	// LIFETIME
	public GaData getScrollRateEventCountToURL(String url, String percentage)
			throws IOException {
		url = makeURLAnalyticsFriendly(url);
		String filter = "ga:pagePath==" + url + ";ga:eventLabel==" + percentage;

		GaData results = analytics
				.data()
				.ga()
				.get("ga:" + PROFILE_ID, "2015-01-01", "today",
						"ga:totalEvents").setFilters(filter).execute();

		return results;
	}

	public GaData getPageviewsToURL(String url) throws IOException {
		url = makeURLAnalyticsFriendly(url);
		return analytics.data().ga()
				.get("ga:" + PROFILE_ID, "2014-08-01", "today", "ga:pageViews")
				.setFilters("ga:pagePath==" + url).execute();
	}

	private String makeURLAnalyticsFriendly(String url) {
		url = url.replace("http://beta.himate.de", "");
		url = url.replace("beta.himate.de", "");
		return url;
	}

	public void examplePrintResults(GaData results) {
		// Parse the response from the Core Reporting API for
		// the profile name and number of sessions.
		if (results != null && !results.getRows().isEmpty()) {
			System.out.println("View (Profile) Name: "
					+ results.getProfileInfo().getProfileName());
			System.out.println("TotalSesions: "
					+ results.getRows().get(0).get(0));
		} else {
			System.out.println("No results found");
		}
	}

	// HttpRequestInitializer for longer timeOut
	private HttpRequestInitializer setHttpTimeout(
			final HttpRequestInitializer requestInitializer) {
		return new HttpRequestInitializer() {
			@Override
			public void initialize(HttpRequest httpRequest) throws IOException {
				requestInitializer.initialize(httpRequest);
				httpRequest.setConnectTimeout(8 * 60000); // 3 minutes connect
															// timeout
				httpRequest.setReadTimeout(8 * 60000); // 3 minutes read timeout
			}
		};
	}
	
}
