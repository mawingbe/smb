package domain.googleanalytics;

import java.io.IOException;

import access.AnalyticsAccess;

import com.google.api.services.analytics.Analytics;
import com.google.api.services.analytics.model.GaData;
import com.google.api.services.pagespeedonline.Pagespeedonline;
import com.google.api.services.pagespeedonline.Pagespeedonline.Builder;
import com.google.api.services.pagespeedonline.model.Result;

public class AnalyticsReportCreator {

	// Get  data from AnalyticsAccess
	private  Analytics analytics;
	private  String PROFILE_ID;
	
	public AnalyticsReportCreator(){
		private  Analytics analytics = AnalyticsAccess.getAnalytics();
		private  String PROFILE_ID = AnalyticsAccess.getPROFILE_ID();
	}

	public  AnalyticsDataForMorningReport getReportData(String URL) {
		if(!URL.contains("beta.himate.de")){
			return new AnalyticsDataForMorningReport(
					0, 0, 0, 0, 0,
					0, 0);
		}

		try {
			int pageviews = getPageviewsToURL(URL);
			int pageviewsBounced = getPageviewsBouncedToURL(URL);
			int baseline = getScrollRateEventCountToURL(URL, "Baseline");
			int fifty = getScrollRateEventCountToURL(URL, "50");
			int seventyfive = getScrollRateEventCountToURL(URL, "75");
			int timeSpent = getTime(URL);
			int pagesize = getPagesize(URL);
			AnalyticsDataForMorningReport data = new AnalyticsDataForMorningReport(
					pageviews, pageviewsBounced, baseline, fifty, seventyfive,
					timeSpent, pagesize);
			return data;

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

	}

	private  int getPagesize(String URL) throws IOException {
		// Builder from AnalyticsAccess
		Builder builder = AnalyticsAccess.getBuilder();
		// Pagespeeonline Object
		Pagespeedonline pso = new Pagespeedonline(builder.getTransport(),
				builder.getJsonFactory(), builder.getHttpRequestInitializer());

		// Get Pagespeedstats to URL
		Result stats = pso.pagespeedapi().runpagespeed(URL).execute();
		Long[] values = new Long[7];
		// Sizes
		values[0] = stats.getPageStats().getCssResponseBytes();
		values[1] = stats.getPageStats().getHtmlResponseBytes();
		values[2] = stats.getPageStats().getImageResponseBytes();
		values[3] = stats.getPageStats().getJavascriptResponseBytes();
		values[4] = stats.getPageStats().getFlashResponseBytes();
		values[5] = stats.getPageStats().getTextResponseBytes();
		values[6] = stats.getPageStats().getOtherResponseBytes();
		int size = 0;
		for (Long i : values) {
			if (i != null) {
				size += i.intValue();
			}
		}
		// Calculate KBs
		return size / 1024;

	}

	private  int getTime(String url) throws IOException {
		url = makeURLAnalyticsFriendly(url);
		// Only request events tagged 'Riveted'
		String filter = "ga:pagePath==" + url + ";ga:eventCategory==Riveted";

		// Take data from last 7 days
		GaData results = analytics.data().ga()
				.get("ga:" + PROFILE_ID, "7daysAgo", "today", "ga:eventValue")
				.setFilters(filter).execute();

		int pageviews = Integer.parseInt(results.getRows().get(0).get(0));
		return pageviews;
	}

	private  int getScrollRateEventCountToURL(String url,
			String percentage) throws IOException {
		url = makeURLAnalyticsFriendly(url);
		// Only request events labeled with xy%
		String filter = "ga:pagePath==" + url + ";ga:eventLabel==" + percentage;

		// Take data from last 7 days
		GaData results = analytics.data().ga()
				.get("ga:" + PROFILE_ID, "7daysAgo", "today", "ga:totalEvents")
				.setFilters(filter).execute();

		int pageviews = Integer.parseInt(results.getRows().get(0).get(0));
		return pageviews;
	}

	private  int getPageviewsToURL(String url) throws IOException {
		url = makeURLAnalyticsFriendly(url);
		// Pageviews - URL
		GaData gadata = analytics.data().ga()
				.get("ga:" + PROFILE_ID, "7daysAgo", "today", "ga:pageViews")
				.setFilters("ga:pagePath==" + url).execute();
		int pageviews = Integer.parseInt(gadata.getRows().get(0).get(0));
		return pageviews;

	}

	private  int getPageviewsBouncedToURL(String url) throws IOException {
		url = makeURLAnalyticsFriendly(url);
		
		//Pageviews from sessions with only one site visited
		GaData gadata = analytics.data().ga()
				.get("ga:" + PROFILE_ID, "7daysAgo", "today", "ga:pageViews")
				.setFilters("ga:pagePath==" + url)
				.setSegment("gaid::5M0VWgz6QpqtInadzeWBpQ").execute();
		int pageviews = Integer.parseInt(gadata.getRows().get(0).get(0));
		return pageviews;

	}

	private  String makeURLAnalyticsFriendly(String url) {
		//Analytics can't deal with full URL
		url = url.replace("http://beta.himate.de", "");
		url = url.replace("beta.himate.de", "");
		return url;
	}

}
