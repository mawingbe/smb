<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" version="1.0" encoding="UTF-8"
		indent="yes" />

	<xsl:template name="report" match="/">
		<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="de">
			<header>
				<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
				<link href="/style/main.css" rel="stylesheet" type="text/css" />
				<title>Daily Report</title>
				<link href='http://fonts.googleapis.com/css?family=Open+Sans'
					rel='stylesheet' type='text/css' />
				<style>
					body{
					font-family: 'Open Sans', sans-serif;
					width: 297mm;
					}

					h1 {
					text-align: center;
					color: black;
					background: #F8973C ;
					font-family:
					'Open Sans', sans-serif;

					}

					#fbimage {

					align : center;
					width: 100mm;


					}

				</style>

			</header>

			<body>
				<table>
					<tr>
						<td>
							<h1>
								Report für "
								<xsl:value-of select="//HEADER/TITEL" />
								"
							</h1>
							<p>
								URL:
								<xsl:value-of select="//HEADER/URL" />
							</p>
						</td>
					</tr>
					<tr>
						<td>
							<h2>Facebook</h2>
							<div class="fbimage">
								<img alt="Bild">
									<xsl:attribute name="src">
				<xsl:value-of select="//FACEBOOKDATA/IMAGEURL" />
				</xsl:attribute>


								</img>
							</div>

							<p>
								<xsl:value-of select="//FACEBOOKDATA/HEADLINE" />
							</p>
							<p>
								Click-Through-Rate:
								<xsl:value-of select="//FACEBOOKDATA/CTR" />
								%
							</p>
						</td>
					</tr>
					<tr>
						<td>
							<h2>Google Analytics</h2>
							<p>
								Pageviews:
								<xsl:value-of select="//ANALYTICS/PAGEVIEWS" />
							</p>
							<p>
								Anteil der Personen, die min. 50% des Artikels gelesen haben:
								<xsl:value-of select="//ANALYTICS/FIFTY" />
								%
							</p>
							<p>
								Anteil der Personen, die min. 75% des Artikels gelesen haben:
								<xsl:value-of select="//ANALYTICS/SEVENTYFIVE" />
								%
							</p>
							<p>
								Durchschnittliche Verweildauer:
								<xsl:value-of select="//ANALYTICS/AVERAGETIMESPENT" />
								Sekunden
							</p>
							<p>
								Absprungrate:
								<xsl:value-of select="//ANALYTICS/BOUNCERATE" />
								%
							</p>
							<p>
								Der Artikel hat eine Größe von
								<xsl:value-of select="//ANALYTICS/PAGESIZE" />
								KB
							</p>
						</td>
					</tr>

				</table>








			</body>


		</html>
	</xsl:template>

</xsl:stylesheet>
